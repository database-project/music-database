/* query1: همه ی ترکهای مربوط به ارتیست با ایدی12 */
SELECT
TrackId,name,AlbumId
from
tracks
where AlbumId in(select AlbumId
				FROM albums
				WHERE
				ArtistId=12
				);
/* query2: ده تا از بزرگترین ترکها*/				
SELECT
trackid, name , bytes
FROM
tracks
ORDER BY bytes DESC
LIMIT 10;
/* query3: ترکهایی که ایدی  انه 1و2و3 نباشد*/
SELECT
TrackId,Name,GenreId
from tracks
WHERE
GenreId not in (1,2,3);
/* query4:  تعداد ترکهای هر البوم*/
SELECT
AlbumId,COUNT(TrackId)
FROM
tracks
GROUP BY AlbumId
ORDER BY COUNT(TrackId) DESC;
/* query5:نام البومهایی که تعداد ترکهای بیشتر از 15 تا دارند*/
SELECT
tracks.AlbumId,Title,COUNT(TrackId)
FROM
tracks
INNER JOIN albums on albums.AlbumId=tracks.AlbumId
GROUP BY tracks.AlbumId
HAVING  COUNT(TrackId)>150
ORDER BY COUNT(TrackId) DESC;
/* query6: ایدی هر البوم به همراه مجموع ثانیه های ترک و مجموع بایت های ترکها */
SELECT
AlbumId,
SUM (Milliseconds) as lenght,
SUM (Bytes) as size
FROM tracks
GROUP BY AlbumId;
/* query7:  اگر مشتری در کشوری غیر از امریکا بود خارجی در نظرگرفته شود*/
SELECT
CustomerId,FirstName,LastName
CASE Country
WHEN 'usa'
THEN 'dosmetic'
ELSE 'FOREIGN'
end as (customer_group)
FROM customers
ORDER BY LastName,FirstName;
/* query8 : مشخص کردن کوتاه و طولانی بودن ترکها و گزاشتن اطلاعات داخل کتگوری */
SELECT
trackid , name , 
CASE
WHEN milliseconds<6000 THEN "short"
WHEN milliseconds>6000 and milliseconds<30000 THEN "medium"
ELSE
"long"
END as
category
FROM
tracks;
/* query9 : نام ترک و البوم هایی که که ژانر راک دارند*/
SELECT
tracks.name , tracks.albumid
from tracks, genres
WHERE genres.name= "rock";
/* query10 : نام و ایدی ترکهایی که توسط مشتریان کشور فرانسه خریداری شده اند */
 SELECT
invoice_items.trackid
FROM invoice_items , customers , invoices
WHERE customers.country = "france"
;